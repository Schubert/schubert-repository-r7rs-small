((retropikzel
   (libraries
     (string-util
       (url . "https://codeberg.org/retropikzel/scheme-string-util.git")
       (description . "Utilities related to string handling"))
     (event
       (url . "https://codeberg.org/Spite/scheme-event.git")
       (description . "Event library"))
     (color
       (url . "https://codeberg.org/Spite/scheme-color.git")
       (description . "Color library"))
     (time
       (url . "https://codeberg.org/Spite/scheme-time.git")
       (description . "Time Library"))
     (geometry
       (url . "https://codeberg.org/Spite/scheme-geometry.git")
       (description . "2D Library for geometry shapes and calculations"))
     (vector-2d
       (url . "https://codeberg.org/Spite/scheme-vector-2d.git")
       (description . "Library for easily using 3d-vectors")))))
