This is a repository for [Schubert](https://schubert.retropikzel.net),
containing libraries that are portable accross R7RS-small implementations.

To add your own library please make pull request.

## Definition of portable

The definition of the word portable when used in this repository means code that
conforms to [R7RS Small](https://small.r7rs.org/attachment/r7rs.pdf) and uses
only [SRFIs](https://srfi.schemers.org) which are in final status.
