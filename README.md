This is a repository for [Schubert](https://schubert.retropikzel.net),
containing libraries that are portable accross R7RS-small implementations.

To add your own library please make pull request.

## Definition of portable

The definition of the word portable when used in this repository means code that
conforms to [R7RS Small](https://small.r7rs.org/attachment/r7rs.pdf) and uses
only [SRFIs](https://srfi.schemers.org) which are in final status.


# Libraries

## (retropikzel string-util v1-0-1)

Utilities related to string handling

Versions: v1-0-0 v1-0-1 


## (retropikzel event v0-1-0)

Event library

Versions: v0-1-0 


## (retropikzel color v0-1-1)

Color library

Versions: v0-1-1 


## (retropikzel time v0-1-0)

Time Library

Versions: v0-1-0 


## (retropikzel geometry v0-1-0)

2D Library for geometry shapes and calculations

Versions: v0-1-0 


## (retropikzel vector-2d v0-2-0)

Library for easily using 3d-vectors

Versions: v0-2-0 



